/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jainch
 */
public class SellingDao {

    static Customer cusArr;
    static Double totalPrice;
    static Selling arr;
    String[] coln = {"ID", "Name", "Amount", "Price"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };

    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_softdev/db/book_softdev.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }

    public DefaultTableModel resetModel() {
        model.setRowCount(0);
        return model;
    }

    public Double getPrice(){
        return arr.getPrice();
    }
    
    public void getSelling(String id, String date, String userId, String customerId) {
        String sql = "INSERT INTO Selling(Selling_id, Selling_date, User_id, Customer_id) VALUES(?,?,?,?)";
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, id);
            pstmt.setString(2, date);
            pstmt.setString(3, userId);
            pstmt.setString(4, customerId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void getDetail(String id, int amount, Double price, String bookId, String sellingId) {
        String sql = "INSERT INTO Selling_detail(Selling_detail_id, Selling_detail_amount, Selling_detail_price, Book_id, Selling_id) VALUES(?,?,?,?,?)";
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, id);
            pstmt.setInt(2, amount);
            pstmt.setDouble(3, price);
            pstmt.setString(4, bookId);
            pstmt.setString(5, sellingId);
            
            pstmt.executeUpdate();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
    }

    public Customer searchCus(String id) {
        String sql = "SELECT Customer_firstname FROM Customer WHERE Customer_tel = ?";
        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, id);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                cusArr.fname = rs.getString("Customer_firstname");
                cusArr.id = rs.getString("Customer_id");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return cusArr;
    }

    public DefaultTableModel searchBook(String id) {
        String sql = "SELECT Book_id, Book_name, Book_amount, Book_price FROM Book WHERE Book_id = ?";

        try (Connection conn = this.connect();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the value
            pstmt.setString(1, id);
            //
            ResultSet rs = pstmt.executeQuery();
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Book_id");
                String Name = rs.getString("Book_name");
                int Amount = rs.getInt("Book_amount");
                Double Price = rs.getDouble("Book_price");
                model.addRow(new Object[]{ID, Name, 1, Price});
            }
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }
}
