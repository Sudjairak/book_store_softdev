
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jainch
 */
public class TimesheetDao {
    String[] coln = {"ID", "Checkin", "Checkout", "User_id"};
    DefaultTableModel model = new DefaultTableModel(coln, 0) {
    };
    
    Connection connect() {
        // SQLite connection string
        String url = "jdbc:sqlite:D:/book_softdev/db/book_softdev.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return conn;
    }
    
    public DefaultTableModel resetModel(){
        model.setRowCount(0);
        return model;
    }
    
    public DefaultTableModel selectForShowTable() {
        String sql = "SELECT * FROM User_log";

        try (Connection conn = this.connect();
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql)) {
            // loop through the result set
            while (rs.next()) {
                String ID = rs.getString("Log_id");
                String Checkin = rs.getString("Log_checkin");
                String Checkout = rs.getString("Log_checkout");
                String UserID = rs.getString("User_id");
                model.addRow(new Object[]{ID, Checkin, Checkout, UserID});
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "alert", JOptionPane.ERROR_MESSAGE);
        }
        return model;
    }
}
