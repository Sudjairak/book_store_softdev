/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jainch
 */
public class Selling {
    String id;
    int amount;
    Double price;
    String bookId;
    String SellingId;
    
    public Selling(String id, int amount, Double price, String bookId, String bookName){
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.bookId = bookId;
        this.SellingId = bookName;
    }
    
    public String getId(){
        return id;
    }
    public int getAmount(){
        return amount;
    }
    public Double getPrice(){
        return price;
    }
    public String getBookId(){
        return bookId;
    }
    public String getSellingId(){
        return SellingId;
    }
    
    public void setId(String id){
        this.id = id;
    }
    public void setAmount(int amount){
        this.amount = amount;
    }
    public void setDouble(Double price){
        this.price = price;
    }
    public void setBookId(String bookId){
        this.bookId = bookId;
    }
    public void setSellingId(String sellingId){
        this.SellingId = sellingId;
    }
}
