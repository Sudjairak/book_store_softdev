

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jainch
 */
public class Book {

    String id;
    String name;
    int amount;
    double price;
    String date;
    String type;

    public Book(String id, String name, int amount, double price, String date, String type) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.date = date;
        this.type = type;
    }

    public String getId() {
        return id;
    }
    
    public String getName() {
        return name;
    }

    public int getAmount(){
        return amount;
    }
    
    public double getPrice(){
        return price;
    }
    
    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public void setName(){
        this.name = name;
    }
    
    public void setAmount(){
        this.amount = amount;
    }
    
    public void setPrice(){
        this.price = price;
    }
    
    public void setDate(){
        this.date = date;
    }
    
    public void setType(){
        this.type = type;
    }
}
