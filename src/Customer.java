

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jainch
 */
public class Customer {

    String id;
    String tel;
    String fname;
    String lname; 
    String gender;
    String type;

    public Customer(String id, String tel, String fname, String lname, String gender, String type) {
        this.id = id;
        this.tel = tel;
        this.fname = fname;
        this.lname = lname;
        this.gender = gender;
        this.type = type;
    }

    public String getId() {
        return id;
    }
    
    public String getTel() {
        return tel;
    }

    public String getFname() {
        return fname;
    }

    public String getLname() {
        return lname;
    }

    public String getGender() {
        return gender;
    }

    public String getType() {
        return type;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public void setLname(String lname) {
        this.lname = lname;

    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setType(String type) {
        this.type = type;
    }
}
